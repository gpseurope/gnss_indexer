#!/usr/bin/env python

"""Usage: populateLog.py [-h] [-s] [-a] [-m] [-r root_path] [-w wrong_path] [-b to_be_indexed_path] [-i indexed_path] [-c station_code] [-C station_list_file] [-l list_file] [-H rhost] [-U ruser] [-P rpassword] [-K private_key] [-n data_center] [-y year] [-o doy] [-d debug] [--verbose] [-e list_excluded]

Options:
   -h --help                                    Show this screen.
   -s --skip_existing                           Skip anubis check for file already indexed
   -a --skip_anubis                             Skip Anubis check
   --version                                    Show version.
   -r --root_path path                          Root path to search files for
   -w --wrong_path path                         Root path where to copy wrong files discarded by indexGD (empty or unreadable)
   -b --to_be_indexed_path path                 Root path where to copy files, processed by indexGD, which were not indexed for any reason
   -i --indexed_path path                       Root path where to copy indexed files
   -n --data_center data_center                 Data center used on indexGD tool
   -y --year year                               Specific year (CCYY)
   -o --doy doy                                 Specific doy
   -d --debug debug                             Debug level [default: WARN]
   -H --rhost host                              Remote host to scan via sftp
   -U --ruser ruser                             Remote host user
   -P --rpassword password                      Remote host password
   -K --private_key key_file                    Private ssh key for remote access
   -c --station_code marker                     Get only the station with this marker
   -C --station_list_file file                  External file with list of stations to get
   -e --list_excluded file                      Add excluded file in this output file
   -m --remove_files                            Remove files from root_path after elaboration
   -l --list_file file                          Get files to index from file list instead of root path

"""

""" 
"""

from docopt import docopt
import datetime
from datetime import datetime, timedelta, date, time
import os
from os.path import expanduser, basename
from os.path import isdir, join
from schema import Schema, And, Or, Use, SchemaError
import logging
import re
import subprocess
import tempfile
import shutil
import requests
import json
import pysftp
from jsoncomment import JsonComment
from ConfigParser import SafeConfigParser
import sys

class GnssIndexer(object):
    def __new__(cls, args, conf):
        self = super(GnssIndexer, cls).__new__(cls, args, conf)

        if self.check_params():
            return self
        return None

    def __init__(self, args, conf):
        self.args = args
        self.conf = conf
        self.logger = self.createLogger()
        self.rinex_obs_file_name = ''
        self.current_rinex_file = None
        self.current_temp_rinex_file = None

        self.number_of_found_files = 0
        self.number_of_elaborated_files = 0
        self.number_of_valid_files = 0
        self.number_of_files_with_wrong_name = 0
        self.number_of_files_in_wrong_folder = 0
        self.number_of_files_not_read_from_indexGD = 0
        self.number_of_updated_files_on_db = 0
        self.number_of_added_files_on_db = 0
        self.number_of_call_to_fwss_service_failed = 0
        self.number_of_call_to_index_service_failed = 0

        # self.total_number_of_found_files = 0
        # self.total_number_of_elaborated_files = 0
        # self.total_number_of_valid_files = 0
        # self.total_number_of_files_with_wrong_name = 0
        # self.total_number_of_files_in_wrong_folder = 0
        # self.total_number_of_filestotal_number_of_added_files_on_db_not_read_from_indexGD = 0
        # self.total_number_of_updated_files_on_db = 0
        # self.total_number_of_added_files_on_db = 0
        # self.total_number_of_call_to_fwss_service_failed = 0

        self.year = ''
        self.doy = ''

        tmpdirname_prefix = 'gnss_indexer_' + self.args["--data_center"]

        self.tmpdirname = tempfile.mkdtemp(prefix=tmpdirname_prefix)
        self.anubis_xml_dir =  os.path.join(self.tmpdirname, 'anubis_log')
        self.logger.info("temporary dir name: [%s]" % (self.tmpdirname))

        if self.args['--list_excluded']:
            open(self.args['--list_excluded'], 'w').close()

        if self.args['--station_list_file']:
            self.marker_list=[]
            with open(self.args['--station_list_file'], 'r') as myfile:
                for line in myfile:
                    self.marker_list.append(line[:4].upper())

    def check_params(self):
        if args['--root_path'] and not os.path.isdir(args['--root_path']):
            print("Error in parameter --root_path. Directory {} does not exists" \
                  .format(args['--root_path']))
            return False

        if args['--wrong_path'] and (not os.path.isdir(args['--wrong_path'])):
            print("Error in parameter --wrong_path. Directory {} does not exists" \
                  .format(args['--wrong_path']))
            return False

        if args['--indexed_path']:
            if not os.path.isdir(args['--indexed_path']):
                print("Error in parameter --indexed_path. Directory {} does not exists" \
                      .format(args['--indexed_path']))
                return False

        if args['--to_be_indexed_path']:
            if not os.path.isdir(args['--to_be_indexed_path']):
                print("Error in parameter --to_be_indexed_path. directory {} does not exists" \
                      .format(args['--to_be_indexed_path']))
                return False

        if args['--list_file']:
            if not self.file_exists(args['--list_file']):
                print("Error in parameter --list_file. File {} does not exists" \
                      .format(args['--list_file']))
                return False

        return True

    def file_exists(self, file):
        return os.path.isfile(file)

    def createLogger(self):
        logger = logging.getLogger('populateLog')
        logger.setLevel(args["--debug"])
        log_dir = '.'
        t = datetime.today();

        if "--year" in self.args and self.args["--year"] is not None:
            year = '-' + self.args["--year"]
        else:
            year = ''

        if "--doy" in self.args and self.args["--doy"] is not None:
            doy = '-' + self.args["--doy"]
        else:
            doy = ''

        if "--station_code" in self.args and self.args["--station_code"] is not None:
            station_code = '-' + self.args["--station_code"]
        elif self.args['--station_list_file']:
            station_code = '_LIST'
        else:
            station_code = ''

        log_file_name = os.path.join(self.conf['LOG']['folder'], "gnss_indexer_{}{}{}{}_{}{}{}_{}{}{}") \
            .format(self.args["--data_center"].upper(),
                    year,
                    doy,
                    station_code,
                    t.year,
                    str(t.month).zfill(2),
                    str(t.day).zfill(2),
                    str(t.hour).zfill(2),
                    str(t.minute).zfill(2),
                    str(t.second).zfill(2))

        log_file_name = os.path.join(log_dir, log_file_name)
        handler = logging.FileHandler(log_file_name)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(handler)
        return logger

    def sortedDirsWalk(self, path):
        # Get the names from inside the path. Get directories
        # into a list and then sort and return it.
        names = os.listdir(path)
        dirs = []
        for name in names:
            if isdir(join(path, name)):
                dirs.append(name)

        dirs.sort()
        return dirs

    def sortedFilesWalk(self, path):
        from os.path import isdir, join

        # Get the names from inside the path. Get the files
        # into a list and then sort and return it.
        names = os.listdir(path)
        nondirs = []
        for name in names:
            if not isdir(join(path, name)):
                nondirs.append(os.path.join(path, name))

        nondirs.sort()
        return nondirs

    def scan_dir(self):
        my_dirs = self.sortedDirsWalk(args["--root_path"])
        for my_dir in my_dirs:
            self.manage_year_folder(os.path.join(args["--root_path"], my_dir))

    def run(self):
        if self.args['--list_file']:
            for i, f in enumerate(open(self.args['--list_file']).read().splitlines()):
                if self.file_exists(f):
                    self.filter_file(f)
                else:
                    self.logger.error("File {} does not exists!!!".format(f))
        else:
            if self.args["--year"]:
                self.manage_year_folder(os.path.join(self.args["--root_path"], self.args["--year"]))
            else:
                self.scan_dir()

            if self.args["--remove_files"]:
                self.remove_empty_dirs(self.args["--root_path"])

        self.print_totals()

    def remove_empty_dirs(self, root):
        for root, dirs, files in os.walk(root, topdown=False):
            for name in dirs:
                try:
                    if len(os.listdir(os.path.join(root, name))) == 0:  # check whether the directory is empty
                        #print("Deleting", os.path.join(root, name))
                        try:
                            os.rmdir(os.path.join(root, name))
                        except:
                            #print("FAILED :", os.path.join(root, name))
                            pass
                except:
                    pass

    def print_totals(self):
        self.logger.warning("==================")
        self.logger.warning("Total Number of [Found] [%d]" % (self.number_of_found_files))
        self.logger.warning("Total Number of [Elaborated] [%d]" % (self.number_of_elaborated_files))
        self.logger.warning("Total Number of [Valid] %d]" % (self.number_of_valid_files))
        self.logger.warning("Total  Number of [Wrong name] [%d]" % (self.number_of_files_with_wrong_name))
        self.logger.warning("Total Number of [Wrong folder] [%d]" % (self.number_of_files_in_wrong_folder))
        self.logger.warning(
            "Total Number of [NOT read from indexGD] [%d]" % (self.number_of_files_not_read_from_indexGD))
        self.logger.warning("Total Number of [DB Updated succeded] [%d]" % (self.number_of_updated_files_on_db))
        self.logger.warning("Total Number of [DB Insert succeded] [%d]" % (self.number_of_added_files_on_db))
        self.logger.warning(
            "Total Number of call to index service failed [%d]" % (self.number_of_call_to_index_service_failed))
        self.logger.warning(
            "Total Number of call to QC service failed [%d]" % (self.number_of_call_to_fwss_service_failed))

    def do_nothing(self, p):
        pass


    def manage_rinex_file(self):
        # run indexGD
        err_msg, indexGD_data = self.call_indexGD()
        if err_msg:
            self.number_of_files_not_read_from_indexGD += 1
            self.write_to_excluded(err_msg)

            if self.args["--wrong_path"]:
                self.copy_file(self.args["--wrong_path"])
            return

        err_msg = self.call_dbapi_index_service(indexGD_data)
        if err_msg:
            self.write_to_excluded(err_msg)
            if self.args["--to_be_indexed_path"]:
                self.copy_file(self.args["--to_be_indexed_path"])
            return

        if self.args["--indexed_path"]:
            self.copy_file(self.args["--indexed_path"])

        if self.args["--skip_anubis"]:
            self.number_of_valid_files += 1
            return

        retVal, xml_data = self.call_anubis()

        if retVal:
            self.call_dbapi_qc_service(xml_data)

        self.number_of_valid_files += 1

        return

    def copy_file(self, dest_path):
        file_name = os.path.basename(self.current_rinex_file)
        destination_path = os.path.join(dest_path, self.year, self.doy)

        if not os.path.isdir(destination_path):
            os.makedirs(destination_path, 0755)
        shutil.copyfile(self.current_rinex_file,
                    os.path.join(destination_path, file_name))

    def delete_file(self):
        os.remove(self.current_rinex_file)

    def call_indexGD(self):
        rinex = self.current_temp_rinex_file

        indexGD_temp_dir = os.path.join(self.tmpdirname, 'indexGD')

        if not os.path.exists(indexGD_temp_dir):
            os.makedirs(indexGD_temp_dir)

        indexGD_input_list_file_full_name = os.path.join(indexGD_temp_dir, 'input_list')

        with open(indexGD_input_list_file_full_name, "w") as text_file:
            text_file.write(rinex)

        error_file_name  = os.path.join(indexGD_temp_dir, 'error_file')
        output_file = os.path.join(indexGD_temp_dir, 'output_file')

        indexGD_folder, indexGD_program = os.path.split(self.conf['Programs']['indexGD'])
        my_args = [
                    self.conf['Programs']['python'],
                    indexGD_program,
                    '-o', output_file,
                    '-l', indexGD_input_list_file_full_name,
                    '-s', '24h',
                    '-f', '30s',
                    '-t', 'RINEX2',
                    '-d', self.args['--data_center'],
                    '-r', '%YYYY%/%DOY%'
                ]
        try:
            self.logger.debug("[{}] Call indexGD".format(rinex))

            result = subprocess.check_call(
                        my_args,
                        stdout=subprocess.PIPE,
                        cwd = indexGD_folder
                    )
        except Exception as e:
            mystring = "subprocess call to indexGD error: {}" \
                              .format(str(e))
            self.logger.error(mystring)
            return mystring, None

        if os.path.isfile(error_file_name):
            mystring = open(error_file_name).read()
            self.logger.error(mystring)
            return mystring, None

        if not os.path.isfile(output_file):
            mystring = "Could not read output file from indexGD: {}" \
                              .format(os.path.basename(rinex))
            self.logger.error(mystring)
            return mystring, None
        try:
            with open(output_file) as json_file:
                parser = JsonComment(json)
                my_list = parser.load(json_file)
            rinex_json = json.dumps(my_list[0])
        except Exception as e:
            mystring = str(e)
            self.logger.error(mystring)
            return mystring, None

        return None, rinex_json

    def call_dbapi_index_service(self, payload):
        rinex = self.current_temp_rinex_file
        # call index service
        url = self.conf['FWSS-API']['url'] + "/gps/data/rinex"
        head = {'Content-type': 'application/json'}

        try:
            res = requests.post(url, data=payload, headers=head)
            if res.status_code not in [200, 201]:
                    mystring = "index service returned the following error message {}. Status code: {}" \
                            .format(res.text, res.status_code)
                    self.logger.error(mystring)
                    self.number_of_call_to_index_service_failed += 1

                    return mystring

            if res.status_code == 200:
                self.number_of_updated_files_on_db += 1
                self.logger.info("Already indexed file {} has been updated" \
                                 .format(os.path.basename(rinex)))
            elif res.status_code == 201:
                self.number_of_added_files_on_db += 1
                self.logger.info("new file {} has been indexed" \
                                 .format(os.path.basename(rinex)))
        except Exception as e:
            self.number_of_call_to_fwss_service_failed +=1
            mystring = "error calling service gps/data/rinex: %s" % str(e)
            self.logger.error(mystring)
            self.number_of_call_to_index_service_failed += 1
            return mystring

        return None


    def write_to_excluded(self, error_msg):
        if self.args['--list_excluded']:
            error_msg = error_msg.replace('\n', ' ').replace('\r', '') + os.linesep
            text = "{} {}".format(self.current_rinex_file,
                                error_msg)
            with open(self.args['--list_excluded'], 'a') as excluded_files:
                excluded_files.write(text)


    def call_anubis(self):
        rinex = self.current_temp_rinex_file
        my_args = [
            self.conf['Programs']["RunQC"],
            '--time_lim', '99',
            'BRD/%Y',
            '-verb', '1',
            '-mask_rxo', rinex,
            '--save_xml',
            '--save_dir',
            self.anubis_xml_dir
        ]

        result = subprocess.check_call(my_args,
                stdout=subprocess.PIPE,
                cwd = os.path.dirname(self.conf['Programs']["RunQC"]))

        anubis_output_xml_file_name = "{}{}{}0.xml".format(self.station_code, self.year, self.doy)
        anubis_output_xml_file_full_name = os.path.join(self.anubis_xml_dir, anubis_output_xml_file_name)

        try:
            with open(anubis_output_xml_file_full_name, 'r') as myfile:
                anubis_xml = myfile.read()
        except Exception as e:
            self.logger.error("[{}] Anubis did not create xml output file expected {}" \
                              .format(rinex, anubis_output_xml_file_full_name))
            return False, None

        return True, anubis_xml

    def call_dbapi_qc_service(self, anubis_xml):
        rinex = self.current_temp_rinex_file
        url = self.conf['FWSS-API']['url'] + "/gps/data/qcfile"
        head = {'Content-type': 'application/xml'}

        try:
            res = requests.post(url, data=anubis_xml, headers=head)
        except Exception as e:
            self.logger.error("error: %s" % str(e))
            return

        self.logger.info("Status [%s] text: [%s]" % (res.status_code, res.text.replace('\n', '')))

        if res.status_code == 200:
            self.logger.debug("qc data of rinex file {} has been updated".format(rinex))
        elif res.status_code == 201:
            self.logger.debug("qc data of rinex file {} has been added".format(rinex))
        else:
            self.number_of_call_to_fwss_service_failed += 1
            self.logger.error("dbapi_qc_service error. File: [{}] Status: {} message: {}" \
                                  .format(rinex, res.status_code, res.text))
        return

    def filter_file(self, rinex):
        self.number_of_found_files += 1

        rinex = rinex.replace('\\', r'\\')

        my_regex = r"(^\w{4})(\d{3})0\.(\d{2})d\.Z$"
        rinex_file_base_name = os.path.basename(rinex)
        match = re.search(my_regex,rinex_file_base_name)
        if not match:
            self.logger.error("Not valid rinex2 format: [%s]" % (rinex))
            self.number_of_files_with_wrong_name += 1
            return

        self.station_code = match.group(1)
        station_doy = match.group(2)
        station_year = match.group(3)

        if args['--list_file']:
            self.doy = match.group(2)
            if int(match.group(3)) > 80:  # this work only from 1981 to 2080
                self.year = '19' + match.group(3)
            else:
                self.year = '20' + match.group(3)
        else:
            if station_doy != self.doy or station_year != self.year[-2:]:
                self.logger.error("rinex file name: [%s] found in wrong folder" % (rinex))
                self.number_of_files_in_wrong_folder += 1
                return

        if self.args["--station_code"] \
                and self.station_code.upper() != self.args["--station_code"].upper():
            return

        if self.args["--station_list_file"] \
                and self.station_code.upper() not in self.marker_list:
            return

        self.current_rinex_file = rinex
        self.manage_file()

    def manage_file(self):
        self.number_of_elaborated_files += 1
        rinex_file_basename = os.path.basename(os.path.normpath(self.current_rinex_file))
        self.current_temp_rinex_file = os.path.join(self.tmpdirname, rinex_file_basename)
        self.get_file(self.current_rinex_file, self.current_temp_rinex_file)

        self.manage_rinex_file()

        if self.args['--remove_files']:
            self.delete_file()

        if self.number_of_found_files > 0 and (self.number_of_found_files % 100) == 0:
            self.logger.warning("Found [%d] files" % (self.number_of_found_files))
            self.logger.warning("Elaborated [%d] files" % (self.number_of_elaborated_files))

        if os.path.exists(self.current_temp_rinex_file):
            os.remove(self.current_temp_rinex_file)

    def get_file(self, source, target):
        shutil.copyfile(source, target)


    def manage_doy_folder(self, folder):
        self.doy = os.path.basename(os.path.normpath(folder))
        self.logger.warning("Year -> [%s] Doy ->[%s]" %( self.year, self.doy))
        if not re.match(r'\d{3}', self.doy):
            return
        self.scan_doy_folder(folder)

    def manage_year_folder(self, folder):
        self.year = os.path.basename(os.path.normpath(folder))
        self.logger.warning("Year ->[%s]" %( self.year))
        if not re.match(r'\d{4}', self.year):
            return

        if self.args["--doy"]:
            doy_folder = os.path.join(folder, self.args["--doy"])
            self.manage_doy_folder(doy_folder)
        else:
            self.scan_year_folder(folder)

    def scan_network_folder(self, folder):
        if not os.path.exists(folder):
            self.logger.warning("folder [%s] does not exists" % (folder))
            return
        #pysftp.walktree(folder, self.do_nothing, self.manage_year_folder, self.do_nothing, False)
        my_dirs = self.sortedDirsWalk(folder)
        for my_dir in my_dirs:
            self.manage_year_folder(os.path.join(folder, my_dir))

    def scan_year_folder(self, folder):
        if not os.path.exists(folder):
            self.logger.warning("folder [%s] does not exists" % (folder))
            return
        #pysftp.walktree(folder, self.do_nothing, self.manage_doy_folder, self.do_nothing, False)
        my_dirs = self.sortedDirsWalk(folder)
        for my_dir in my_dirs:
            self.manage_doy_folder( os.path.join(folder, my_dir))

    def scan_doy_folder(self, folder):
        if not os.path.exists(folder):
            self.logger.warning("folder [%s] does not exists" % (folder))
            return
        #pysftp.walktree(folder, self.manage_file, self.do_nothing, self.do_nothing, False)
        my_files = self.sortedFilesWalk(folder)
        for my_file in my_files:
            self.filter_file(my_file)

class GnssIndexerRemote(GnssIndexer):
    def __new__(cls, args, conf):
        self = super(GnssIndexer, cls).__new__(cls, args, conf)
        self.args = args
        try:
            cnopts = None

            connection_args = { 'host': args["--rhost"],
                                'username': args["--ruser"],
                                'cnopts': cnopts
                            }

            if args['--private_key']:
                connection_args['private_key'] = args['--private_key']

            if args['--rpassword']:
                connection_args['password'] = args['--rpassword']

            self.srv = pysftp.Connection(**connection_args)

        except Exception as e:
            print(repr(e))
            return None

        if self.check_params():
            return self

        return None

    def __init__(self, args, conf):
        GnssIndexer.__init__(self, args, conf)

    def check_params(self):
        try:
            self.srv.chdir(args['--root_path'])
        except IOError:
            print("parameter --root_path. Directory {} does not exists" \
                  .format(args['--root_path']))
            return False

        if args['--wrong_path']:
            try:
                self.srv.chdir(args['--wrong_path'])
            except IOError:
                print("parameter --wrong_path. Directory {} does not exists" \
                      .format(args['--wrong_path']))
                return False

        if args['--indexed_path']:
            try:
                self.srv.chdir(args['--indexed_path'])
            except IOError:
                print("parameter --indexed_path. Directory {} does not exists" \
                      .format(args['--indexed_path']))
                return False

        if args['--to_be_indexed_path']:
            try:
                self.srv.chdir(args['--to_be_indexed_path'])
            except IOError:
                print("parameter --to_be_indexed_path. Directory {} does not exists")
                return False

        if args['--list_file']:
            if not self.file_exists(args['--list_file']):
                print("parameter --list_file. File {} does not exists" \
                      .format(args['--list_file']))
                return False
        return True

    def file_exists(self, file):
        try:
            self.srv.stat(file)
            return True
        except IOError:
            return False

    def scan_dir(self):
        self.srv.walktree(args["--root_path"], self.do_nothing, self.manage_year_folder(), self.do_nothing, False)

    def get_file(self, source, target):
        self.srv.get(source, target)

    def copy_file(self, dest_path):
        file_name = os.path.basename(self.current_rinex_file)
        destination_path = os.path.join(dest_path, self.year, self.doy)

        try:
            self.srv.chdir(destination_path)  # Test if remote_path exists
        except IOError:
            self.srv.makedirs(destination_path)  # Create remote_path

        self.srv.put(self.current_temp_rinex_file,
                    os.path.join(destination_path, file_name))

    def delete_file(self):
        self.srv.remove(self.current_rinex_file)


    def scan_doy_folder(self, folder):
        folder.replace('\\', '/')
        try:
            self.srv.chdir(folder)
        except IOError as e:
            self.logger.warning("folder [%s] does not exists" % (folder))
            return
        self.srv.walktree(folder, self.filter_file, self.do_nothing, self.do_nothing, False)

    def scan_year_folder(self, folder):
        folder.replace('\\', '/')
        try:
            self.srv.chdir(folder)
        except IOError as e:
            self.logger.warning("folder [%s] does not exists" % (folder))
            return

        self.srv.walktree(folder, self.do_nothing, self.manage_doy_folder, self.do_nothing, False)

    def scan_network_folder(self, folder):
        #print ("prima->" + folder)
        folder = folder.replace('\\', '/')
        #print ("dopo->" + folder)
        try:
            self.srv.chdir(folder)
        except IOError as e:
            self.logger.warning("folder [%s] does not exists" % (folder))
            return
        self.srv.walktree(folder, self.do_nothing, self.manage_year_folder, self.do_nothing, False)

    def delete_empy_dir(self, dir):
        if len(self.srv.listdir_attr(dir)) == 0:
            self.delete_empty_dir_list.append(dir)

    def remove_empty_dirs(self, root):
        self.delete_empty_dir_list = []
        self.srv.walktree(root, self.do_nothing, self.delete_empy_dir, self.do_nothing, True)
        while self.delete_empty_dir_list:
            for d in self.delete_empty_dir_list:
                self.srv.rmdir(d)
            self.delete_empty_dir_list = []
            self.srv.walktree(root, self.do_nothing, self.delete_empy_dir, self.do_nothing, True)


def parseConfig(type =""):
    '''
    Config parser that returns necessary parameters for the program to run
    '''

    try:
        configFile = "gnss_indexer.cfg"
        parser = SafeConfigParser()

        if not os.path.isfile(configFile):
            print '''>> Error in parseConfig(): Config file "{0}" does not exist.'''.format(configFile)
            print '''>> Please provide a valid config file. See default file "gnss_indexer.cfg.default"'''
            print '''>> rename it to gnss_indexer.cfg and configure for your environment.'''
            sys.exit(0)

        conf_data = {
            'Programs': {},
            'FWSS-API': {},
            'LOG': {}
        }

        parser.read(configFile)

        conf_data['Programs']['indexGD'] = parser.get('Programs', 'indexGD')
        conf_data['Programs']['RunQC'] = parser.get('Programs', 'RunQC')

        try:
            conf_data['Programs']['python'] = parser.get('Programs', 'python')
        except Exception as e:
            conf_data['Programs']['python'] = 'python'

        conf_data['FWSS-API']['url'] = parser.get('FWSS-API', 'url')
        conf_data['LOG']['folder'] = parser.get('LOG', 'folder')

        return conf_data

    except Exception, e:
        print >> sys.stderr, "Error in parseConfig(): "
        print >> sys.stderr, e
        sys.exit(0)

if __name__ == '__main__':
    args = docopt(__doc__)

    # if args["--debug"] not in ("INFO", "WARN", "ERROR"):
    #     args["--debug"] = "WARN"

    schema = Schema({
        '--verbose': Or(False, True),
        '--help': Or(False, True),
        '--remove_files': Or(False, True),
        '--skip_existing': Or(False, True),
        '--skip_anubis': Or(False, True),
        '--data_center':  Or(And(str, len),
            error = '--data_center option is mandatory'),
        '--year':  Or(None, And(str, len)),
        '--doy':  Or(None, And(str, len)),
        '--rhost':  Or(None, And(str, len)),
        '--ruser':  Or(None, And(str, len)),
        '--rpassword':  Or(None, And(str, len)),
        '--private_key':  Or(None, And(str, len)),
        '--debug': And(str, len),
        '--root_path':  Or(None, And(str, len)),
        '--wrong_path':  Or(None, And(str, len)),
        '--to_be_indexed_path':  Or(None, And(str, len)),
        '--indexed_path':  Or(None, And(str, len)),
        '--list_excluded':  Or(None, And(str, len)),
        '--station_code':  Or(None, And(str, len)),
        '--station_list_file':  Or(None, And(str, len)),
        '--list_file': Or(None, And(str, len))
    })

    try:
        args = schema.validate(args)
    except SchemaError as e:
        print ("error an parameter validation: %s" % str(e))
        exit(0)

    if args["--rhost"] != None  or args["--ruser"] != None or args["--rpassword"] != None or args["--private_key"] != None:
        if (args["--rhost"] is None or args["--ruser"]  is None):
            print ("parameters rhost and ruser are mandatory for remote acces ... ")
            exit(0)

        if args["--rpassword"]  is None and args["--private_key"] is None:
            print ("At lest one of rpassword or private_key parameters must be provided for remote acces ...")
            exit(0)

    if args['--station_list_file']:
        if not os.path.isfile(args['--station_list_file']):
            print("file {} does not exist ".format(args['--station_list_file']))
            exit(0)
        if args['--station_code']:
            print("Parameters --station_list_file and --station_code are not compatible together")
            exit(0)

    if not args['--list_file'] and  not args['--root_path']:
        print("At least one of the two parameters --list_file and --root_path must be provided")
        exit(0)

    if args['--list_file']:
        if args['--root_path']:
            print("Parameters --list_file and --root_path are not compatible together")
            exit(0)
        if args['--year']:
            print("Parameters --list_file and --year are not compatible together")
            exit(0)
        if args['--doy']:
            print("Parameters --list_file and --doy are not compatible together")
            exit(0)

    if args['--doy']:
        args['--doy'] = args['--doy'].zfill(3)

    conf_data = parseConfig()
    # check configuration params
    if not os.path.isfile(conf_data['Programs']['indexGD']):
        print("Program {} indicated in configuration param [Programs][indexGD] param does not exists" \
              .format(conf_data['Programs']['indexGD']))
        exit(3)

    if not os.path.isfile(conf_data['Programs']['RunQC']):
        print("Program {} indicated in configuration param [Programs][RunQC] param does not exists" \
              .format(conf_data['Programs']['RunQC']))
        exit(3)

    if args["--rhost"] is None:
        scanner = GnssIndexer(args, conf_data)
    else:
        scanner = GnssIndexerRemote(args, conf_data)

    if scanner:
        scanner.run()

        if scanner.tmpdirname and os.path.exists(scanner.tmpdirname):
            shutil.rmtree(scanner.tmpdirname)
