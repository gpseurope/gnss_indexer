# 1. Introduction



**Author:** Sergio Bruni (sergio.bruni@ingv.i)

**Document created:** 29. May 2019

**Last updated:** 29. May 2019

## 1.2 Files in the project

The following files are maintained in the archive:

```
cfg.json.template                               template of config file (cfg.json)
gnss_indexer.py                                 code
README.md		                                This readme file you are reading.
```

# 2. Installation

## 2.1 Python

You need Python 2.7 and the Python modules *docopt, schema and pysftp* installed.

Python 3.x will not be supported without some refactoring of the code.

First of you might need to install pip on your linux system:

```
sudo apt-get install python-pip
```

Then you should be able to install the python packages:

```
pip install docopt
pip install schema
pip install pysftp
```

## 2.2 gnss_indexer

copy the file gnss_indexer.cfg.template to gnss_indexer.cfg and modify it for your needs

## 2.3 run

./gnss_indexer.py -h for usage

example:

./gnss_indexer.py -r /data/archive/rinex/CGPS/NETS -n ABRUZZO -y 2019 -o 002
